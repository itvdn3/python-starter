'''
Задание 3
Откройте IDLE (под Windows и OS X это приложение устанавливается вместе с Python).
Поэкспериментируйте с обычными арифметическими выражениями.
Попробуйте задать имя значению какого-либо выражения.
Попробуйте вывести значение выражения с поясняющим текстом при помощи функции print,
используя как имена, так и непосредственно выражения в качестве параметров функции.
'''

print(f"Addition")
a = 1; b = 2; c = a + b
print(f"the result of {a} + {b} is {c}, which is a {type(c)}")
a = 1.0; b = 2; c = a + b
print(f"the result of {a} + {b} is {c}, which is a {type(c)}")
print(f"")

print(f"Subtraction")
a = 10; b = 2; c = a - b
print(f"the result of {a} - {b} is {c}, which is a {type(c)}")
a = 10.0; b = 2; c = a - b
print(f"the result of {a} - {b} is {c}, which is a {type(c)}")
print(f"")

print(f"Multiplication")
a = 5; b = 4; c = a * b
print(f"the result of {a} * {b} is {c}, which is a {type(c)}")
a = 5.0; b = 4; c = a * b
print(f"the result of {a} * {b} is {c}, which is a {type(c)}")
print(f"")

print(f"Division")
try:
    a = 15; b = 3; c = a / b
    print(f"the result of {a} / {b} is {c}, which is a {type(c)}")
    a = 15.0; b = 3; c = a / b
    print(f"the result of {a} / {b} is {c}, which is a {type(c)}")
    a = 15; b = 0; c = a / b
    print(f"the result of {a} / {b} is {c}, which is a {type(c)}")
except ZeroDivisionError:
    print(f"the result of {a} / {b} is 'division by zero'")
print(f"")

print(f"Integer Division")
try:
    a = 15; b = 3; c = a // b
    print(f"the result of {a} // {b} is {c}, which is a {type(c)}")
    a = 15.5; b = 3; c = a // b
    print(f"the result of {a} // {b} is {c}, which is a {type(c)}")
    a = -15.5; b = 3; c = a // b
    print(f"the result of {a} // {b} is {c}, which is a {type(c)}")
    a = -15.5; b = 0; c = a // b
    print(f"the result of {a} // {b} is {c}, which is a {type(c)}")
except ZeroDivisionError:
    print(f"the result of {a} // {b} is 'division by zero'")
print(f"")

print(f"Exponents")
a = 15; b = 3; c = a ** b
print(f"the result of {a} ** {b} is {c}, which is a {type(c)}")
a = 15; b = 0.5; c = a // b
print(f"the result of {a} ** {b} is {c}, which is a {type(c)}")
a = 15; b = -1.2; c = a ** b
print(f"the result of {a} ** {b} is {round(c,2)}, which is a {type(c)}")
print(f"")
print(f"The Modulus Operator")
try:
    a = 15; b = 3; c = a % b
    print(f"the result of {a} % {b} is {c}, which is a {type(c)}")
    a = 15.5; b = 3; c = a % b
    print(f"the result of {a} % {b} is {c}, which is a {type(c)}")
    a = 1; b = 0; c = a % b
    print(f"the result of {a} % {b} is {c}, which is a {type(c)}")
except ZeroDivisionError:
    print(f"the result of {a} % {b} is 'modulo by zero'")
print(f"")
'''
To calculate the remainder r of dividing a number a by a number b, 
Python uses the equation r = a - (b * (a // b)).
'''
a = 15.5; b = -3; c = a % b
print(f"the result of {a} % {b} is {c}, which is a {type(c)}")
a = -15.5; b = 3; c = a % b
print(f"the result of {a} % {b} is {c}, which is a {type(c)}")
a = -15.5; b = -3; c = a % b
print(f"the result of {a} % {b} is {c}, which is a {type(c)}")
print(f"")

print(f"Math Functions")
print(f"Round Numbers With round()")
import math
try:
    a = math.pi; b = 2; c = round(a, b)
    print(f"the result of round({a:.5f},{b}) is {c}, which is a {type(c)}")
    a = math.pi; b = 2.5; c = round(a, b)
    print(f"the result of round({a:.5f},{b}) is {c}, which is a {type(c)}")
except TypeError:
    print(f"the result of round({a:.5f},{b}) is 'float' object cannot be interpreted as an integer")
print(f"")
print(f"Find the Absolute Value With abs()")
a = 15; c = abs(a)
print(f"the result of abs({a}) is {c}, which is a {type(c)}")
a = -15.5; c = abs(a)
print(f"the result of abs({a}) is {c}, which is a {type(c)}")
print(f"")

print(f"Raise to a Power With pow()")
a = 15; b = 2; c = pow(a, b)
print(f"the result of pow({a},{b}) is {c}, which is a {type(c)}")
a = -15.5; b = 2; c = pow(a, b)
print(f"the result of pow({a},{b}) is {c}, which is a {type(c)}")
a = 15; b = 2; c = 3; d = pow(a, b, c) #pow(a, b, c) is equivalent to (a ** b) % c.
print(f"the result of pow({a},{b},{c}) is {d}, which is a {type(d)}")
print(f"")

print(f"Complex Numbers")
a = 15 + 5j; b = 20 + 10j
print(f"a complex number is {a}")
print(f"the real component of the number is {a.real}, which is a {type(a.real)}")
print(f"the imaginary component of the number is {a.imag}, , which is a {type(a.imag)}")
try:
    print(f"the result of {a} + {b} is {a + b}, which is a {type(a + b)}")
    print(f"the result of {a} - {b} is {a - b}, which is a {type(a - b)}")
    print(f"the result of {a} * {b} is {a * b}, which is a {type(a * b)}")
    print(f"the result of {a} / {b} is {a / b}, which is a {type(a / b)}")
    print(f"the result of {a} // {b} is {a / b}, which is a {type(a // b)}")
except TypeError:
    print(f"the result of {a} // {b} is can\'t take floor of complex number")
