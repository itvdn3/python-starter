'''
Задание 4
Создайте новый проект в интегрированной среде разработки PyCharm.
Создайте файл исходного кода и напишите программу, которая выводит ваше имя.
Запустите его.
'''

name = "Yana"
profession = "student"
affiliation = "CyberBionic Systematics Education System"
message = (
    f"My name is {name}. " 
    f"I am a {profession}. " 
    f"I study Python at {affiliation}. "
)
print(message)
