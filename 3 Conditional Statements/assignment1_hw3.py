'''
Задание 1
Напишите программу, которая спрашивает
у пользователя его имя, и если оно совпадает с вашим,
выдает определенное сообщение.
'''

my_name: str = "yana".title()
user_name = input("What is your name? ")

if user_name.title().strip() == my_name:
    print(f"You are my namesake")
else:
    print(f"Our names are different")
