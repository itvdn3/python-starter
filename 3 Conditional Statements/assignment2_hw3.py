'''
Задание 2
Напишите программу, которая вычисляет значение функции
в диапазоне значений x от -10 до 10 включительно с шагом, равным 1.

y = x2 при -5 <= x <= 5;
y = 2*|x|-1 при x < -5;
y = 2x при x > 5.

Вычисление значения функции оформить в виде программной функции,
которая принимает значение x, а возвращает полученное значение функции (y).
'''


def function_value(our_range):
    values = []
    for x in our_range:
        if -5 <= x and x <= 5:
            y = x ** 2
        elif x <= -5:
            y = 2 * abs(x) - 1
        else:
            y = 2 * x
        values.append(y)
    return values


print([value for value in range(-10, 11)])
print(function_value(range(-10, 11)))
