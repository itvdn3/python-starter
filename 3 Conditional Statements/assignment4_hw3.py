'''
Задача 4.
Напишите программу-калькулятор, в которой пользователь
сможет ввести выбрать операцию, ввести необходимые числа
и получить результат. Операции, которые необходимо реализовать:
сложение, вычитание, умножение, деление, возведение в степень,
синус, косинус и тангенс числа.
'''


import math

print("Функція tan")
print("Область визначення тангенса - R, окрім pi/2 + pi*k, k - ціле число")
print("Для тестування цього винятку введіть: ")
print(f"{math.pi/2} або {3*math.pi/2} або {5*math.pi/2} і т.д "
      f"з періодом {math.pi} ")
print(f"{-math.pi/2} або {-3*math.pi/2} або {-5*math.pi/2} і т.д "
      f"з періодом {math.pi} ")
print(f"")

x = float(input('Enter number x: '))
y = float(input('Enter number y: '))
operation = input('Enter arithmetic operation (+, -, *, /, ^) or '
                  'math function (sin, cos, tan): ')

result = None

if operation == '+':
    result = x + y
    print(f"{x} {operation} {y} = {result}")
elif operation == '-':
    result = x - y
    print(f"{x} {operation} {y} = {result}")
elif operation == '*':
    result = x * y
    print(f"{x} {operation} {y} = {result}")
elif operation == '/':
    result = x / y
    print(f"{x} {operation} {y} = {result}")
elif operation == '^':
    result = x ** y
    print(f"{x} {operation} {y} = {result}")
elif operation == 'sin':
    result = math.sin(x), math.sin(y)
    print(f"({operation}({x:.4f}),{operation}({y:.4f})) = ({result}))")
elif operation == 'cos':
    result = math.cos(x), math.cos(y)
    print(f"({operation}({x:.4f}),{operation}({y:.4f})) = ({result})")
elif operation == 'tan':
    if math.tan(x + 0.005) < 0 and math.tan(x - 0.005) > 0:
        print(f'tan({x}) = inf')
    else:
        result = math.tan(x)
        print(f'tan({x}) = {result}')
    if math.tan(y + 0.005) < 0 and math.tan(y - 0.005) > 0:
        print(f'tan({y}) = inf')
    else:
        result = math.tan(y)
        print(f'tan({y}) = {result}')
else:
    print('Unsupported operation')
