'''
Задание 3
Используя вложенные циклы и функции print(‘*’, end=’’),
print(‘ ‘, end=’’) и print() выведите на экран
прямоугольный треугольник.
'''

for i in range(1, 11):
    print()
    for j in range(1, i):
        print('*', end='')
    print(' ', end='')
print()
