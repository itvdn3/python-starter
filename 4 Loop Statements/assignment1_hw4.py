'''
Задание 1
Даны числа a и b (a < b). Выведите сумму всех
натуральных чисел от a до b (включительно).
'''

import math

a = float(input("Введіть дійсне число a (додатне або від'ємне): "))
b = float(input("Введіть дійсне число b (додатне або від'ємне)"
                " так, щоб a < b: "))

number = math.ceil(a)
cum_sum = 0

while number <= 0:
    number += 1
else:
    while number <= math.floor(b):
        cum_sum = cum_sum + number
        number += 1
    else:
        # print("Loop completed successfully.")
        if a <= 0 and b <= 0:
            print(f"Натуральних чисел між від'ємними числами не існує")
        elif a > b:
            print(f"Невиконана умова задачі. Число {a} більше числа {b}")
        else:
            print(f"Сума всіх натуральних чисел між {a} та {b} включно "
                  f"дорівнює ", cum_sum)
