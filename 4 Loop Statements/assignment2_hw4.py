'''
Задание 2
Факториалом числа n называется число 𝑛! = 1 ∙ 2 ∙ … ∙ 𝑛.
Создайте программу, которая вычисляет факториал
введённого пользователем числа.
'''

number = int(input("Введіть натуральне число: "))

factorial = 1

if number < 0:
    print("Число повинно бути додатним.")
elif number == 0:
    print(f"Факторіал {number} дорівнює {factorial}")
elif number > 0:
    for i in range(1, number+1):
        factorial = factorial * i
    print(f"Факторіал {number} дорівнює {factorial}")
