'''
Задание 1
Прочитайте в документации по языку Python информацию
о перечисленных в резюме данного урока стандартных функциях.
Проверьте их на практике.
'''

# All
bool_exps = [5 > 2, 1 == 1, 42 < 50] #  All items evaluate as true.
print(all(bool_exps))
print('\n')
objects = ["Hello!", 42, {}] #  One item evaluates as false.
print(all(objects))
print('\n')
general_exps = [5 ** 2, 42 - 3, int("42")] #  All items evaluate as true.
print(all(general_exps))
print('\n')
empty = [] #  The input iterable is empty.
print(all(empty))
print('\n')
# With tuples
print(all((1, 2, 3)))
print(all((0, 1, 2, 3)))
print(all(()))
print(all(tuple()))
print('\n')
# With range objects
print(all(range(10)))
print(all(range(1, 11)))
print(all(range(0)))
print('\n')
# With dictionary
medals = {"gold": 1, "silver": 2, "bronze": 3}
print(all(medals.keys()))
print('\n')
numbers = {0: "zero", 1: "one", 2: "two"}
print(all(numbers.keys()))
print('\n')
monday_inventory = {"book": 2, "pencil": 5, "eraser": 1}
print(all(monday_inventory.values()))
print('\n')
tuesday_inventory = {"book": 2, "pencil": 3, "eraser": 0}
print(all(tuesday_inventory.values()))
print('\n')
# With list
numbers = [1, 2, 3]
print(all([number > 0 for number in numbers]))
print('\n')

numbers = [-2, -1, 0, 1, 2]
print(all([number > 0 for number in numbers]))
print('\n')

#Map()
str_nums = ["4", "8", "6", "5", "3", "2", "8", "9", "2", "5"]
int_nums = map(int, str_nums)
print(next(int_nums))
print(next(int_nums))
print('\n')

numbers = [-2, -1, 0, 1, 2]
abs_values = map(abs, numbers)
print(next(abs_values))
print(next(abs_values))
print('\n')

float_values = map(float, numbers)
print(next(float_values))
print(next(float_values))
print('\n')

words = ["Welcome", "to", "Real", "Python"]
len_words = map(len, words)
print(list(map(len, words)))
print('\n')

numbers = [1, 2, 3, 4, 5]
squared = map(lambda num: num ** 2, numbers)
print(list(squared))
print('\n')

first_it = [1, 2, 3]
second_it = [4, 5, 6, 7]
print(list(map(pow, first_it, second_it)))
print('\n')
print(list(map(lambda x, y, z: x + y + z, [2, 4], [1, 3], [7, 8])))
print('\n')

string_it = ["processing", "strings", "with", "map"]
print(list(map(str.capitalize, string_it)))
print(list(map(str.upper, string_it)))
print(list(map(str.lower, string_it)))
print('\n')

with_spaces = ["processing ", "  strings", "with   ", " map   "]
print(list(map(str.strip, with_spaces)))
print('\n')

with_dots = ["processing..", "...strings", "with....", "..map.."]
print(list(map(lambda s: s.strip("."), with_dots)))
print('\n')

import re
def remove_punctuation(word):
    return re.sub(r'[!?.:;,"()-]', "", word)

print(remove_punctuation("...Python!"))
print('\n')
