'''
Задание 2
Создайте программу, которая проверяет,
является ли палиндромом введённая фраза.
'''

import re


def remove_punctuation(word):
    return re.sub(r'[!?.:;,"()-]', "", word)


def palindrom(phrase):
    phrase_copy = phrase.copy()
    phrase.reverse()
    if phrase_copy == phrase:
        return print(f"Ваша фраза паліндром")
    else:
        return print(f"Ваша фраза не є паліндромом")


print("Нижче пропонуються фрази паліндроми "
      "для тестування коду або введіть вашу фразу")
print()
print("Was it a car or a cat I saw?")
print("Live on time, emit no evil")
print("Do geese see God?")
print("І що сало? Ласощі")
print("кит на морі романтик")
print()
text = input("Ваша фраза: ")

list_of_chars_forward = list(remove_punctuation(text.lower()))

for char in list_of_chars_forward:
    if char == ' ':
        list_of_chars_forward.remove(char)

palindrom(list_of_chars_forward)
