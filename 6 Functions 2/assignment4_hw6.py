'''
Задание 4
Напишите рекурсивную функцию, которая вычисляет
сумму натуральных чисел, которые входят в заданный промежуток.
'''

import math

print("Вам необхідно ввести два дійсних числа a і b так, щоб a < b")
a = float(input("Введіть дійсне число a (додатне або від'ємне): "))
b = float(input("Введіть дійсне число b (додатне або від'ємне): "))


def cum_sum_recursion(num, step):
    if num <= 0:
        return 0
    if num <= math.ceil(a):
        print('step', step, ': number ', num)
        return math.ceil(a)
    else:
        print('step', step, ': number ', num)
    return num + cum_sum_recursion(num - 1, step + 1)


print(f"Сума всіх натуральних чисел, які входять "
      f"в заданий проміжок [{a}; {b}] дорівнює ",
      cum_sum_recursion(math.floor(b), 1))
