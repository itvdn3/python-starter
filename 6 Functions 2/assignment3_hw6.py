'''
Задание 3
Пусть на каждую ступеньку лестницы можно стать
с предыдущей или переступив через одну.
Определите, сколькими способами можно подняться
на заданную ступеньку.
'''

number = int(input("Введіть номер сходинки: "))


def step_recursion(num):
    if num == 1:
        return 1
    if num == 2:
        return 2
    return step_recursion(num - 1) + step_recursion(num - 2)


print(f"Піднятися на {number} сходинку можна "
      f"{step_recursion(number)} способами")
