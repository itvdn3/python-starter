text = """
The Zen of Python, by Tim Peters

Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one -- and preferably only one -- obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than right now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!
"""

print("The Frequency of Words")
dict_words = {}
list_of_words = text.lower().replace(',', ' ').replace('.', ' ').replace('--', ' ').replace('!', ' ').split()

for word in list_of_words:
    dict_words[word] = list_of_words.count(word)

for word in sorted(dict_words, key=dict_words.get, reverse=True):
    print(word, dict_words[word])
print()

print("The Frequency of Characters")
dict_char = {}
list_of_char = list(text.replace('\n', ' '))

for char in list_of_char:
    dict_char[char] = list_of_char.count(char)

for char in sorted(dict_char, key=dict_char.get, reverse=True):
    print(char, dict_char[char])
