'''
Задание 2
Создайте две функции, вычисляющие значения определённых
алгебраических выражений. Выведите на экран таблицу значений
этих функций от -5 до 5 с шагом 0.5.
'''

import math


def func1(x):
    try:
        return x ** 2/(math.sqrt(3 - x) - math.sqrt(3 + x))
    except ValueError:
        return Exception("undefined")
    except ZeroDivisionError:
        return math.inf


def func2(x):
    try:
        return (1 - x)/(1 + x)
    except ZeroDivisionError:
        return math.inf


x = -5
while x <= 5:
    if type(func1(x)) == float:
        print(f"{x} {func1(x):.2f} {func2(x):.2f}")
    else:
        print(f"{x} {func1(x)} {func2(x):.2f}")
    x += 0.5
