'''
Задание 3
Создайте программу-калькулятор, которая поддерживает
четыре операции: сложение, вычитание, умножение, деление.
Все данные должны вводиться в цикле, пока пользователь
не укажет, что хочет завершить выполнение программы.
Каждая операция должна быть реализована в виде отдельной функции.
Функция деления должна проверять данные на корректность
и выдавать сообщение об ошибке в случае попытки деления на ноль.
'''
import math


def addition(a, b):
    return a + b


def subtraction(a, b):
    return a - b


def product(a, b):
    return a * b


def division(a, b):
    try:
        return a / b
    except ZeroDivisionError:
        return math.inf


print("Enter 'q' at any time to quit.")
while True:
    x = input('Enter number x: ')
    if x == 'q':
        break
    y = input('Enter number y: ')
    if y == 'q':
        break
    operation = input("Enter arithmetic operation (+, -, *, /): ")
    if operation == 'q':
        break

    if operation == '+':
        result = addition(float(x), float(y))
    elif operation == '-':
        result = subtraction(float(x), float(y))
    elif operation == '*':
        result = product(float(x), float(y))
    elif operation == '/':
        result = division(float(x), float(y))
    print(f"\t{x} {operation} {y} = {result:.2f}")
