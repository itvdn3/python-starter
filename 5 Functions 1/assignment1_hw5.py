'''
Задание 1
Создайте функцию, которая выводит приветствие
для пользователя с заданным именем. Если имя
не указано, она должна выводить приветствие
для пользователя с Вашим именем.
'''


def greeting(x):
    return print(f"Hello, {x}. Nice to meet you.")


name = input("Hello. I don't think we've met. What is your name? ")

if name == "":
    name = "Yana"

greeting(name)
