'''
Задание 4
Создайте программу, которая состоит из функции,
которая принимает три числа и возвращает их среднее
арифметическое, и главного цикла, спрашивающего
у пользователя числа и вычисляющего их средние
значения при помощи созданной функции.
'''


def mean(a, b, c):
    return (a + b + c) / 3


print("Enter 'q' at any time to quit.")
while True:
    x = input('Enter first number x: ')
    if x == 'q':
        break
    y = input('Enter second number y: ')
    if y == 'q':
        break
    z = input('Enter third number z: ')
    if z == 'q':
        break

    result = mean(float(x), float(y), float(z))
    print(f"\t {result:.2f}")
