'''
Задание 4
Напишите программу, которая запрашивает у пользователя
радиус круга и выводит его площадь.
'''


import math


def get_float_number(number):
    """Опрацювання винятку ValueError"""
    active = True
    while active:
        try:
            f = float(input(
                f"Please enter a {number} (an integer or a float number): "))
            if f <= 0.0:
                raise ValueError
            active = False
        except ValueError:
            print('You must enter a POSITIVE integer or float number')
    return f


r = get_float_number('radius')
S = math.pi * (r**2)
print(f"An area of a circle is {S:.2f}")
