'''
Задание 1
Напишите программу, которая спрашивает у пользователя
два слова и выводит их разделёнными запятой.
'''


def get_words(first_word, second_word):
    both_words = f"{first_word}, {second_word}"
    return both_words


first = input("Please enter a first word: ")
second = input("Please enter a second word: ")
print(f"Words separated by comma are: {get_words(first, second)}")
