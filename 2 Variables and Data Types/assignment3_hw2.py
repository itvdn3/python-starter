'''
Задание 3
Напишите программу, которая решает квадратное уравнение
по формулам. Значения a, b и c вводятся с клавиатуры.
Для извлечения корня используйте оператор возведения в степень,
а не функцию math.sqrt, чтобы получить комплексные числа в случае,
если подкоренное выражение отрицательно.
'''


def get_float_number(number):
    active = True
    while active:
        try:
            f = float(input(
                f"Please enter {number} (an integer or a float number): "))
            if number == 'a' and f == 0.0:
                raise ZeroDivisionError
            active = False
        except ValueError:
            print('You must enter an integer or a float number')
        except ZeroDivisionError:
            print('You must enter a number IS NOT equal zero')
    return f


a = get_float_number('a')
b = get_float_number('b')
c = get_float_number('c')
D = b ** 2 - 4 * a * c
if D > 0:
    print(f"Рівняння має два корені ")
    x1 = (-b + (b ** 2 - 4 * a * c) ** 0.5)/(2 * a)
    x2 = (-b - (b ** 2 - 4 * a * c) ** 0.5)/(2 * a)
    print(f"x1 = {x1:.2f}, x2 = {x2:.2f}")
elif D == 0:
    print(f"Рівняння має один корінь ")
    x1 = x2 = -b / (2 * a)
    print(f"x1 = x2 = {x1:.2f}")
else:
    print(f"Рівняння дійсних розв'язків немає ")
    x1 = (-b + (b ** 2 - 4 * a * c) ** 0.5)/(2 * a)
    x2 = (-b - (b ** 2 - 4 * a * c) ** 0.5)/(2 * a)
    print(f"x1 = {x1:.2f}, x2 = {x2:.2f}")
