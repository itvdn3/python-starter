'''
Задание 2
Напишите программу, которая запрашивает три целых числа a, b и x и
выводит True, если x лежит между a и b, иначе – False.
'''


def get_integer_number(ordinal_number):
    active = True
    while active:
        try:
            c = int(input(f"Please enter a {ordinal_number} integer number: "))
            active = False
        except ValueError:
            print('You must enter an integer number')
    return c


a = get_integer_number('first')
b = get_integer_number('second')
x = get_integer_number('third')
print(f"True: {a} < {x} < {b}") if a < x and x < b else print("False")
