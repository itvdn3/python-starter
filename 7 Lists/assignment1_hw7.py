'''
Задание 1
Создайте список и введите его значения.
Найдите наибольший и наименьший элемент списка,
а также сумму и среднее арифметическое его значений.
'''

my_list = list(range(1, 10))

# зірочка *args вказує, що аргументи мають бути зібрані в кортеж
def min_value(*args):
    return min(args)


def max_value(*args):
    return max(args)


def sum_value(*args):
    return sum(args)


def mean_value(*args):
    return sum(args)/len(args)


# зірочка * вказує, що список має бути розпакований
# та елементи передані в функцію як окремі аргументи
print(f"Мінімальне значення: {min_value(*my_list)}")
print(f"Максимальне значення: {max_value(*my_list)}")
print(f"Сума: {sum_value(*my_list)}")
print(f"Середнє значення: {mean_value(*my_list)}")
