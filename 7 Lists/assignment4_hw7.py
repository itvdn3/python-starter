'''
Задание 4
Создайте список, введите количество его
элементов и сами значения, выведите
эти значения на экран в обратном порядке.
'''

print("Початковий список елементів")
my_list = [20, "abc", 7.5, -50, 'ABC']
print(my_list)


def reverse_list(ll):
    t = []
    for value in range(1, len(ll) + 1):
        t.append(ll[-value])
    return t


print("Список елементів в оберненому порядку")
print(reverse_list(my_list))
