'''
Задание 3
Простым называется число, которое делится
нацело только на единицу и само себя.
Число 1 не считается простым. Напишите программу,
которая находит все простые числа в заданном промежутке,
выводит их на экран, а затем по требованию пользователя
выводит их сумму либо произведение.
'''


def check_prime_number(num):
    if num < 2:
        print(f"Число повинно бути більше або дорівнювати 2.")
        return False

    factors = [(1, num)]
    i = 2

    while i * i <= num:
        if num % i == 0:
            factors.append((i, num//i))
        i += 1

    if len(factors) > 1:
        print(f"{num} не є простим числом. Число має дільники: {factors}")
        return False
    else:
        print(f"{num} є простим числом.")
        return True


print("Вам необхідно ввести два натуральних числа a і b так, щоб a < b")
a = int(input("Введіть число a: "))
b = int(input("Введіть число b: "))

list_prime = []

for number in range(a, b + 1):
    bool_prime = check_prime_number(number)
    if bool_prime is True:
        list_prime.append(number)

print(f"")
print(f"Список простих чисел, які входять до проміжку [{a}, {b}]: ")
print(list_prime)
print(f"")

product = 1
operation = input("Введіть операцію, "
                  "яку необхідно виконати з простими числами (+ або *): ")

if operation == "+":
    print(f"Сума дорівнює: {sum(list_prime)}")
elif operation == "*":
    for i in list_prime:
        product = product * i
    print(f"Добуток дорівнює: {product}")
else:
    print("Некоректно введена операція")
