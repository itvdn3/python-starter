# Python Starter

## Completed homeworks assignments

### Topics

1. Introduction to Python

2. Variables and Data Types

3. Conditional Statements

4. Loop Statements

5. Functions 1

6. Functions 2

7. Lists

8. PEP8

9. Miscellania
